# -*- coding: utf-8 -*-
"""
Created on 24 Jun 24.06.17 14:43 2017 

@author: trafrro

Description:

This script will do a text summarization and a keyword extraction based on the gensim modul

"""

from gensim.summarization import summarize, keywords
from utilities.utils import read_text, parse_document

##############################
# GLOBAL VARIABLE DEFINITION
##############################
FILE = "data/trumpet.txt"       # define original corpus
RATIO = 0.4                     # define the summarization ration in % of the original corpus

def textSum_gensim(text, ratio=0.5):
    summary = summarize(text, split=True, ratio=ratio)
    for sentence in summary:
        print sentence,

texttoy = read_text(FILE)
sentences = parse_document(texttoy)
text = ' '.join(sentences)

### Print original document
print "Original Text:"
print "*********************"
for sentence in sentences:
    print sentence


### Print text keywords
print "\nKeywords:"
print "*********************"
print keywords(text)


### Summarize and print summary
print "\nSummarized Text:"
print "*********************"
textSum_gensim(text, ratio=RATIO)