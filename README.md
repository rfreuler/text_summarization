# README #

These scripts are written with python 2.7 (anaconda2)

Prerequisits:

* sklearn
* gensim
* networkx
* scipy
* nltk
* pattern
* HTMLParser


### textSumGensim.py ###
This script will do a text summarization and a keyword extraction based on the gensim modul

### textSumLSA.py ###
Text Summarization using LSA (Latent Semantic Analysis)

* utilizing document sentences
* utilizing the terms in each sentence of the document
* applying SVD to them using some sort of feature weights like Bag of Words or TF-IDF weights.

### textSumTextRank.py ###
Text Summarization using TextRank (based on the PageRank algorithm)



#### Helperfiles:
All modules can be found under the utilities folder

* utilities/contractoins.py
* utilities/utils.py