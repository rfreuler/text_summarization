# -*- coding: utf-8 -*-
"""
Created on 27 Jun 27.06.17 10:09 2017 

@author: trafrro

Description:

Text Summarization using TextRank (based on the PageRank algorithm)

following computation will be done:
    1. Tokenize and extract sentences from the document to be summarized.
    2. Decide on the number of sentences k that we want in the final summary.
    3. Build document term feature matrix using weights like TF-IDF or Bag of Words.
    4. Compute a document similarity matrix by multiplying the matrix with its transpose.
    5. Use these documents (sentences in our case) as the vertices and the similarities between each pair of documents as the weight or score coefficient mentioned earlier and feed them to the PageRank algorithm.
    6. Get the score for each sentence.
    7. Rank the sentences based on score and return the top k sentences.

"""

import matplotlib.pyplot as plt
import networkx
import numpy as np
from pprint import pprint
from utilities.utils import read_text, build_feature_matrix, parse_document

##############################
# GLOBAL VARIABLE DEFINITION
##############################
FILE = "data/trumpet.txt"       # define original corpus
NUM_SENTENCES = 4              # define the number of sentences in the end result
NUM_TOPICS = 2                  # define the number of topics in the end result
NGRAM = 1                       # define NGRAM lenght
SV_THRESHOLD = 0.5              # define salience threshold, all below will be ignored
FEATURE_TYPE = "tfidf"          # define the matrix model (frequency / tfidf / binary)
PLOT_GRAPH = True               # true if we want to plot the sentence similarity graph


# read and prepare the text corpus
texttoy = read_text(FILE)
sentences = parse_document(texttoy)
text = ' '.join(sentences)

# build the sentence-term feature matrix
vec, dt_matrix = build_feature_matrix(sentences, feature_type=FEATURE_TYPE, ngram=NGRAM)

# Compute a document similarity matrix by multiplying the matrix with its transpose.
similarity_matrix = (dt_matrix * dt_matrix.T)
print "SENTENCE SIMILARITY MATRIX:"
print np.round(similarity_matrix.todense(), 2)

# Generate the sentence similarity graph
similarity_graph = networkx.from_scipy_sparse_matrix(similarity_matrix)


# Feed the similarity graph to PageRank algorithm
scores = networkx.pagerank(similarity_graph)
ranked_sentences = sorted(((score, index) for index, score in scores.items()),reverse=True)
print "\nSORTED SENTENCES BY TEXTRANK:"
pprint(ranked_sentences)

# get document summary by combining the above sentences
top_sentence_indices = [ranked_sentences[index][1]
                        for index in range(NUM_SENTENCES)]
top_sentence_indices.sort()

print "\nTOP SENTENCE INDEX POSITION:"
print top_sentence_indices

print "\nDOCUMENT SUMMARY:"
for index in top_sentence_indices:
    print sentences[index],

# Plot the sentence similarity graph
if PLOT_GRAPH:
    networkx.draw_networkx(similarity_graph)
    plt.show()