# -*- coding: utf-8 -*-
"""
Created on 25 Jun 25.06.17 06:54 2017 

@author: trafrro

Description:
Text Summarization using LSA (Latent Semantic Analysis)

	• utilizing document sentences
	• utilizing the terms in each sentence of the document
	• applying SVD to them using some sort of feature weights like Bag of Words or TF-IDF weights.

following computation will be done:
	1. Get the sentence vectors from the matrix V (k rows).
	2. Get the top k singular values from S.
	3. Apply a threshold-based approach to remove singular values that are less than half of the largest singular value if any exist. This is a heuristic, and you can play around with this value if you want.
	4. Multiply each term sentence column from V squared with its corresponding singular value from S also squared, to get sentence weights per topic.
	5. Compute the sum of the sentence weights across the topics and take the square root of the final score to get the salience scores for each sentence in the document.

"""

import numpy as np
from pandas import DataFrame
from scipy.sparse.linalg import svds
from utilities.utils import read_text, build_feature_matrix, parse_document

##############################
# GLOBAL VARIABLE DEFINITION
##############################
FILE = "data/trumpet.txt"       # define original corpus
NUM_SENTENCES = 4              # define the number of sentences in the end result
NUM_TOPICS = 2                  # define the number of topics in the end result
NGRAM = 1                       # define NGRAM lenght
SV_THRESHOLD = 0.5              # define salience threshold, all below will be ignored
FEATURE_TYPE = "frequency"      # define the matrix model (frequency / tfidf / binary)


def low_rank_svd(matrix, singular_count=2):
    u, s, vt = svds(matrix, k=singular_count)
    return u, s, vt


# read and prepare the text corpus
texttoy = read_text(FILE)
sentences = parse_document(texttoy)
text = ' '.join(sentences)

# build the sentence-term feature matrix
vec, dt_matrix = build_feature_matrix(sentences, feature_type=FEATURE_TYPE, ngram=NGRAM)

# convert to term-sentence matrix
td_matrix = dt_matrix.transpose()
td_matrix = td_matrix.multiply(td_matrix > 0)
print "TERM-TO-SENTENCE MATRIX M (MODEL: %s)" % FEATURE_TYPE
if FEATURE_TYPE == 'tfidf':
    print(DataFrame(td_matrix.A, index=vec.get_feature_names()).to_string())
else:
    print(DataFrame(td_matrix.A, index=vec.get_feature_names(), dtype=int).to_string())

# 1) SVD: dimensionality reduction using SVD
u, s, vt = low_rank_svd(td_matrix, singular_count=NUM_TOPICS)

# 2) and 3) remove singular values below threshold
min_sigma_value = max(s) * SV_THRESHOLD
s[s < min_sigma_value] = 0  # set to zero if s is below threshold

# 4) and 5) compute salience scores for all sentences in document
salience_scores = np.sqrt(np.dot(np.square(s), np.square(vt)))
print "\nSALIENCE SCORES FOR EACH SENTENCE:"
print np.round(salience_scores, 2)

# rank sentences based on their salience scores
top_sentence_indices = salience_scores.argsort()[-NUM_SENTENCES:][::-1]
top_sentence_indices.sort()
print "\nTOP SENTENCE INDEX POSITION:"
print top_sentence_indices

# get document summary by combining the above sentences
print "\nDOCUMENT SUMMARY:"
for index in top_sentence_indices:
    print sentences[index],


